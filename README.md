# Kali NetHunter Utility Package

This package, [nethunter-utils](https://pkg.kali.org/pkg/nethunter-utils), includes various custom scripts for the [Kali NetHunter filesystem/chroot](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-rootfs).
