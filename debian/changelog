nethunter-utils (1.6.2) kali-dev; urgency=medium

  [Robin]
  * Upgrade audio streaming script

  [Ben Wilson]
  * Bump release 

 -- Ben Wilson <g0tmi1k@kali.org>  Wed, 20 Nov 2024 11:05:00 +0000

nethunter-utils (1.6.1) kali-dev; urgency=medium

  * Drop upstream

 -- Ben Wilson <g0tmi1k@kali.org>  Mon, 14 Oct 2024 11:56:30 +0100

nethunter-utils (1.6-0kali1) kali-dev; urgency=medium

  * Upstream update

 -- Ben Wilson <g0tmi1k@kali.org>  Mon, 14 Oct 2024 10:49:58 +0100

nethunter-utils (1.5-5) kali-dev; urgency=medium

  * Fix Python shebang in patch for hid-all.py

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 10 May 2024 23:26:56 +0700

nethunter-utils (1.5-4kali1) kali-dev; urgency=medium

  * Update csv2sqlite.py to latest upstream version (python3 support)
  * Port hid-all.py to Python 3
  * Depend on Python 3

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 10 May 2024 22:23:28 +0700

nethunter-utils (1.5-3) kali-dev; urgency=medium

  [ Steev Klimaszewski ]
  * d/watch: Update for changes to GitLab download page changes.

  [ Arnaud Rebillout ]
  * Misc packaging updates
  * Drop python2 dependency for armel and armhf

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 10 May 2024 18:07:41 +0700

nethunter-utils (1.5-2) kali-dev; urgency=medium

  * d/watch: Update url to check for updates.
  * ci: Allow piuparts to fail.
  * Bump standards version to 4.6.2, no changes needed.
  * Update copyright
  * Update synopsis
  * Prepare for Release

 -- Steev Klimaszewski <steev@kali.org>  Mon, 22 May 2023 18:17:49 -0500

nethunter-utils (1.5-1) kali-dev; urgency=medium

  * Update uploaders
  * Add lintian-overrides
  * Add python2 as dependency
  * New upstream version 1.5

 -- Carsten Boeving <re4son@kali.org>  Sat, 12 Nov 2022 16:02:36 +1100

nethunter-utils (1.4-1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Add Uploaders
  * Remove template comment and switch spaces to tabs

  [ Carsten Boeving ]
  * Update watch file
  * New upstream version 1.3
  * New upstream version 1.4

 -- Carsten Boeving <re4son@kali.org>  Thu, 10 Nov 2022 11:17:19 +1100

nethunter-utils (1.1-1kali2) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Ben Wilson ]
  * Fix email address

  [ Sophie Brun ] 
  * Use debhelper-compat 13
  * Bump Standards-Version to 4.5.1

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Sep 2021 09:51:35 +0200

nethunter-utils (1.1-1kali1) kali-dev; urgency=medium

  * Upstream update.

 -- Mati Aharoni <muts@kali.org>  Fri, 02 Sep 2016 19:45:47 -0400

nethunter-utils (1.0-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Ben Wilson <g0tmi1k@kali.org>  Wed, 13 Apr 2016 09:57:12 -0400

nethunter-utils (0.9-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Fri, 01 Jan 2016 08:34:58 -0500

nethunter-utils (0.8-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Thu, 31 Dec 2015 07:36:21 -0500

nethunter-utils (0.7-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Thu, 17 Dec 2015 19:12:08 -0500

nethunter-utils (0.6-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Thu, 17 Dec 2015 09:53:19 -0500

nethunter-utils (0.5-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Mon, 30 Nov 2015 07:29:43 -0500

nethunter-utils (0.4-1kali1) kali-dev; urgency=medium

  * Upstream update

 -- Ben Wilson <g0tmi1k@kali.org>  Sun, 15 Nov 2015 07:00:22 -0500

nethunter-utils (0.3-1kali1) kali-dev; urgency=medium

  * Version bump

 -- Mati Aharoni <muts@kali.org>  Wed, 11 Nov 2015 13:32:03 -0500

nethunter-utils (0.1-1kali1) kali-dev; urgency=low

  * Initial release 

 -- Mati Aharoni <muts@kali.org>  Wed, 11 Nov 2015 10:25:38 -0500
